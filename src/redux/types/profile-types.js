export const ADD_POST = "ADD-POST";
export const EMPTY_INPUT = "EMPTY-INPUT";
export const UPDATE_NEW_POST = "UPDATE-NEW-POST";
export const SET_USER_PROFILE = "SET-USER-PROFILE";
export const SET_STATUS = "SET-STATUS";