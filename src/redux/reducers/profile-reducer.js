import { ADD_POST, EMPTY_INPUT, UPDATE_NEW_POST, SET_USER_PROFILE, SET_STATUS } from '../types/profile-types';

const initialState = {
  posts: [
    { id: 1, message: "How are you", likesCount: 0 },
    { id: 2, message: "its my first post", likesCount: 23 },
    { id: 3, message: "good morning", likesCount: 11 },
  ],
  newPostText: "add post",
  profile: null,
  status: "",
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST:
      return {
        ...state,
        posts: [
          ...state.posts,
          {
            id: Date.now(),
            message: state.newPostText,
            likesCount: 0
          }
        ],
      };
    case UPDATE_NEW_POST:
      return {
        ...state,
        newPostText: action.newText,
      };
    case EMPTY_INPUT:
      return {
        ...state,
        newPostText: '',
      };
    case SET_USER_PROFILE:
      return { ...state, profile: action.profile };
    case SET_STATUS:
      return { ...state, status: action.status };
    default:
      return state;
  }
};



export default profileReducer;
