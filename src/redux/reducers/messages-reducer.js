import { SEND_MESSAGE } from '../types/messages-types';

const initialState = {
  dialogs: [
    { id: 1, name: "Vika" },
    { id: 2, name: "Andrey" },
    { id: 3, name: "Georgy" },
    { id: 4, name: "Vasy" },
  ],
  messages: [
    { id: 1, message: "Hi" },
    { id: 2, message: "How is your it-kamasutra" },
    { id: 3, message: "Yo" },
  ],
  newMessageText: "",
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE:
      return {
        ...state,
        newMessageText: '',
        messages: [
          ...state.messages,
          { id: Date.now(), message: action.message },
        ],
      };
    default:
      return state;
  }
};

export default messagesReducer;
