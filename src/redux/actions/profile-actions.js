import { usersAPI, profileAPI } from '../../API/API';
import { ADD_POST, EMPTY_INPUT, UPDATE_NEW_POST, SET_USER_PROFILE, SET_STATUS } from '../types/profile-types';

// actions
export const addPostAction = () => ({ type: ADD_POST });
export const emptyInputAction = () => ({ type: EMPTY_INPUT });
export const setUsersProfile = (profile) => ({ type: SET_USER_PROFILE, profile });
export const setStatus = (status) => ({ type: SET_STATUS, status });
export const onPostChangeAction = (text) => {
  return { type: UPDATE_NEW_POST, newText: text };
};
// thunks
export const getUsersProfile = (userID) => (dispatch) => {
  usersAPI.getProfile(userID).then((response) => {
    dispatch(setUsersProfile(response.data));
  });
};
export const getStatus = (userID) => (dispatch) => {
  profileAPI.getStatus(userID).then((response) => {
    dispatch(setStatus(response.data));
  });
};
export const updateStatus = (status) => (dispatch) => {
  profileAPI.updateStatus(status).then((response) => {
    if(response.data.resultCode === 0){
      dispatch(setStatus(status));
    };
  });
};

