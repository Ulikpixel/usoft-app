import { SEND_MESSAGE } from '../types/messages-types';

export const sendMessage = (message) => ({ type: SEND_MESSAGE, message });
