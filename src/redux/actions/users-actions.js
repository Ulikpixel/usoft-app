import { usersAPI } from '../../API/API';
import { 
    FOLLOW, UNFOLLOW, 
    SET_USERS, SET_CURRENT_PAGE, 
    SET_TOTAL_USERS, TOGGLE_IS_FETCHING, 
    TOGGLE_FOLLOWING_PROGRESS 
} from '../types/users-types';

export const acceptFollow = (userID) => ({ type: FOLLOW, userID });
export const acceptUnfollow = (userID) => ({ type: UNFOLLOW, userID });
export const setUsers = (users) => ({ type: SET_USERS, users });
export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage });
export const setUsersTotal = (totalCount) => ({ type: SET_TOTAL_USERS, totalCount });
export const setIsFetching = (isFetching) => ({ type: TOGGLE_IS_FETCHING, isFetching });
export const toggleFollowingProgress = (isFetching, userID) => ({ type: TOGGLE_FOLLOWING_PROGRESS, isFetching, userID });

export const getUsers = (currentPage, pageSize) => {
    return (dispatch) => {
        dispatch(setCurrentPage(currentPage))
        dispatch(setIsFetching(true));
        usersAPI.getUsers(currentPage, pageSize).then((data) => {
            dispatch(setIsFetching(false));
            dispatch(setUsers(data.items));
            dispatch(setUsersTotal(data.totalCount));
        });
    };
};

export const follow = (id) => (dispatch) => {
    dispatch(toggleFollowingProgress(true, id));
    usersAPI.follow(id).then((response) => {
        const data = response.data;
        if (data.resultCode === 0) {
            dispatch(acceptFollow(id));
        }
        dispatch(toggleFollowingProgress(false, id));
    });
};


export const unfollow = (id) => (dispatch) => {
    dispatch(toggleFollowingProgress(true, id));
    usersAPI.unfollow(id).then((response) => {
        const data = response.data;
        if (data.resultCode === 0) {
            dispatch(acceptUnfollow(id));
        }
        dispatch(toggleFollowingProgress(false, id));
    });
};

