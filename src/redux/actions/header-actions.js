import { authAPI } from '../../API/API';
import { SET_USER_DATA } from '../types/header-types';

const setAuthUserData = (userID, email, login) => ({ type: SET_USER_DATA, data: { userID, email, login } });
export const getAuthUserData = () => (dispatch) => {
    authAPI.me().then((data) => {
        if (data.resultCode === 0) {
            const { id, login, email } = data.data;
            dispatch(setAuthUserData(id, email, login));
        };
    });
};