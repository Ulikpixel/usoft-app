import * as axios from 'axios';

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': "b32cce1c-b42f-49f9-84c1-3456e0c965e9",
    },
});

export const profileAPI = {
    getProfile(userID) {
        return instance.get("profile/" + userID);
    },
    getStatus(userID) {
        return instance.get("profile/status/" + userID);
    },
    updateStatus(status) {
        return instance.put("profile/status", { status });
    },
};

export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 10) {
        return instance.get(`users?page=${currentPage}&count=${pageSize}`)
            .then((response) => response.data);
    },
    follow(userID) {
        return instance.post(`follow/${userID}`);
    },
    unfollow(userID) {
        return instance.delete(`follow/${userID}`);
    },
    getProfile(userID) {
        return profileAPI.getProfile(userID);
    },
};

export const authAPI = {
    me() {
        return instance.get("auth/me").then((response) => response.data);
    },
};