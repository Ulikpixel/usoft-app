import React from 'react';
import { Redirect } from 'react-router-dom';

export const authRedirect = (Component) => {
    function RedirectComponent(props) {
        if (props.isAuth) {
            return <Redirect to={"/login"} />
        } else {
            return <Component {...props} />
        }
    }
    return RedirectComponent;
};

