import React, { useEffect } from "react";
import man from "../../assets/img/man.jpg";
import s from "./users.module.css";
import { NavLink } from "react-router-dom";
import Preloader from "../common/preloader/preloader";

function Users({
  currentPage,
  users,
  follow,
  unfollow,
  followingProgress,
  getUsers,
  pageSize,
  isFetching,
  totalUsers,
}) {
  const pagesCount = Math.ceil(totalUsers / pageSize);
  const pages = [];
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }
  useEffect(() => {
    getUsers(currentPage, pageSize);
  }, [Users]);
  const onPageChanged = (pageNumber) => {
    getUsers(pageNumber, pageSize);
  };
  return (
    <>
      {isFetching ? <Preloader /> : null}
      <section className={s.users}>
        <h1 className={s.title}>Users</h1>
        <ul className={s.dots}>
          {pages.map((item) => {
            return (
              <li
                key={item}
                className={
                  item === currentPage ? `${s.item} ${s.active}` : s.item
                }
                onClick={() => onPageChanged(item)}
              >
                {item}
              </li>
            );
          })}
        </ul>
        <div className={s.wrapper}>
          {users.map(({ id, photos, followed, name, status }) => {
            return (
              <div key={id} className={s.box}>
                <div className={s.user}>
                  <NavLink to={"/profile/" + id}>
                    <img src={photos.small ? photos.small : man} alt="user" />
                  </NavLink>
                  {followed ? (
                    <button
                      disabled={followingProgress.some((uid) => uid === id)}
                      className={s.unfollow}
                      onClick={() => unfollow(id)}
                    >
                      Unfollow
                    </button>
                  ) : (
                    <button
                      disabled={followingProgress.some((uid) => uid === id)}
                      className={s.follow}
                      onClick={() => follow(id)}
                    >
                      Follow
                    </button>
                  )}
                </div>
                <div className={s.description}>
                  <div className={s.info}>
                    <p className={s.name}>{name}</p>
                    <p className={s.status}>{status}</p>
                  </div>
                  <div className={s.location}>
                    <p className={s.country}>{"location.country"}</p>
                    <p className={s.city}>{"location.city"}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
}

export default Users;
