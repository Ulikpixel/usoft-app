import { connect } from "react-redux";
import React from "react";
import Users from "./users";
import {
  follow,
  unfollow,
  toggleFollowingProgress,
  getUsers,
} from "../../redux/actions/users-actions";
import { authRedirect } from "../../hoc/AuthRedirect";
import { compose } from "redux";

const mapStateToProps = (state) => {
  return {
    users: state.usersPage.users,
    pageSize: state.usersPage.pageSize,
    totalUsers: state.usersPage.totalUsers,
    currentPage: state.usersPage.currentPage,
    isFetching: state.usersPage.isFetching,
    followingProgress: state.usersPage.followingProgress,
  };
};

const UsersContainer = compose(
  authRedirect,
  connect(mapStateToProps, {
    follow,
    unfollow,
    toggleFollowingProgress,
    getUsers,
  }),
)(Users);

export default UsersContainer;
