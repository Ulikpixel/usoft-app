import React from "react";
import { Field, reduxForm } from "redux-form";
import s from "./messagesForm.module.css";

function MessagesForm({ handleSubmit }) {
  return (
    <form className={s.field} onSubmit={handleSubmit}>
      <Field
        placeholder="Enter your message"
        name="newMessageText"
        component="textarea"
      ></Field>
      <button className={s.add__message}>
        SEND
      </button>
    </form>
  );
};

const MessagesReduxForm = reduxForm({ form: "addMessage" })(MessagesForm);

export default MessagesReduxForm;
