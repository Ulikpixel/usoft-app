import React from "react";
import s from "./messages.module.css";
import DialogsItem from "./dialogsitem/dialogsitem";
import MessagesItem from "./messagesItem/messagesItem";
import MessagesReduxForm from "./messagesForm/messagesForm";
import { Redirect } from "react-router-dom";

function Messages({ state, isAuth, sendMessage }) {
  const addMessage = (values) => {
    sendMessage(values.newMessageText);
    values.newMessageText = '';
  };
  if (!isAuth) return <Redirect to={"/login"} />;
  return (
    <section className={s.wrapper}>
      <div className={s.dialogsItems}>
        {state.dialogs.map((dialog, index) => (
          <DialogsItem key={index} name={dialog.name} id={dialog.id} />
        ))}
      </div>
      <div className={s.messages}>
        <div>
          {state.messages.map((item, index) => (
            <MessagesItem key={index} message={item.message} />
          ))}
          <MessagesReduxForm onSubmit={addMessage}/>
        </div>
      </div>
    </section>
  );
}

export default Messages;
