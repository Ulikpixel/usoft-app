import Messages from "./messages";
import { sendMessage } from "../../redux/actions/messages-actions";
import { connect } from "react-redux";
import { authRedirect } from "../../hoc/AuthRedirect";
import { compose } from "redux";

const mapStateToProps = (state) => {
  return {
    state: state.messagesPage,
    isAuth: state.auth.isAuth,
  };
};

const MessagesContainer = compose(
  authRedirect,
  connect(mapStateToProps, { sendMessage }),
)(Messages);

export default authRedirect(MessagesContainer);

