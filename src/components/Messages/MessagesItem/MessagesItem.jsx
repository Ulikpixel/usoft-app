import React from 'react';
import s from './messagesItem.module.css';

function MessagesItem(props) {
    return (
        <div className={s.message}>{props.message}</div>
    )
}

export default MessagesItem
