import React from "react";
import { reduxForm } from "redux-form";
import s from "./login.module.css";
import LoginForm from "./loginForm/loginForm";

const LoginReduxForm = reduxForm({
  form: "login",
})(LoginForm);

function Login() {
  const getForm = (formData) => {
    console.log(formData)
  };
  return (
    <div>
      <h1 className={s.title}>Log in</h1>
      <LoginReduxForm onSubmit={getForm} />
    </div>
  );
}

export default Login;
