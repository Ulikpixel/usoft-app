import React from "react";
import s from "./loginForm.module.css";
import { Field } from "redux-form";
import { required, maxLength, minLength } from "../../../utils/validators/validators";

const maxLength25 = maxLength(25);
const minLength2 = minLength(2);

function LoginForm({ handleSubmit }) {
  return (
    <form className={s.form} onSubmit={handleSubmit}>
      <Field
        type="Email"
        className={s.email}
        name="login"
        component="input"
        validate={[required, maxLength25, minLength2]}
      />
      <Field
        type="Password"
        className={s.password}
        name="password"
        component="input"
      />
      <Field
        type="checkbox"
        className={s.checkbox}
        name="checkbox"
        component="input"
      />
      <button className={s.submit}>Log in</button>
    </form>
  );
}

export default LoginForm;
