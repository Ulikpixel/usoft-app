import React from "react";
import s from "./profileInfo.module.css";
import Preloader from "../../common/preloader/preloader";
import man from "../../../assets/img/man.jpg";
import ProfileStatus from "./profileStatus";

function Profileinfo(props) {
  const { profile } = props;
  if (!profile) {
    return <Preloader />;
  }
  return (
    <div className={s.info}>
      <img src={profile.photos.large ? profile.photos.large : man} alt="img" />
      <p>{profile.fullName}</p>
      <ProfileStatus {...props} />
    </div>
  );
}

export default Profileinfo;
