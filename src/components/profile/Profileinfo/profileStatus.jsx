import React, { useState } from "react";
import s from "./profileInfo.module.css";

function ProfileStatus({ status, updateStatus }) {
  const [active, setActive] = useState(false);
  const [localStatus, setLocalStatus] = useState(status);
  const toggleStatus = (status) => {
    setActive(!active);
    updateStatus(status);
  };
  return (
    <>
      <div className={s.status}>
        <p
          className={active ? s.description : `${s.description} ${s.active}`}
          onDoubleClick={() => setActive(!active)}
        >
          {localStatus ? localStatus : "у вас нет статуса!"}
        </p>
        <input
          type="text"
          className={active ? `${s.field} ${s.active}` : s.field}
          onMouseOut={(e) => toggleStatus(e.target.value)}
          value={localStatus}
          onChange={(e) => setLocalStatus(e.target.value)}
        />
      </div>
    </>
  );
}

export default ProfileStatus;
