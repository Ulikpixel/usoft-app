import React from "react";
import Profile from "./profile";
import { connect } from "react-redux";
import { getUsersProfile, getStatus, updateStatus } from "../../redux/actions/profile-actions";
import { withRouter } from "react-router-dom";
import { authRedirect } from '../../hoc/AuthRedirect';
import { compose } from "redux";

const mapStateToProps = (state) => {
  return {
    profile: state.profilePage.profile,
    isAuth: state.auth.isAuth,
    status: state.profilePage.status,
  };
};

const ProfileContainer = compose(
  authRedirect,
  connect(mapStateToProps, { getUsersProfile, getStatus, updateStatus }),
  withRouter,
)(Profile);

export default ProfileContainer;

