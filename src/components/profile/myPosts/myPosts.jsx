import React from "react";
import s from "./myPosts.module.css";
import Post from "./post/post";

function Posts({ addPost, newPostText, onPostChange, posts }) {
  return (
    <div className={s.posts}>
      <div className={s.wrapper}>
        <input
          type="text"
          className={s.field}
          value={newPostText}
          onChange={(e) => onPostChange(e.target.value)}
        />
        <button className={s.btn} onClick={addPost}>
          ADD POST
        </button>
      </div>
      {posts.map((post, index) => (
        <Post key={index} message={post.message} likesCount={post.likesCount} />
      ))}
    </div>
  );
}

export default Posts;
