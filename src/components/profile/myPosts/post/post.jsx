import React from 'react';
import s from './post.module.css';
import man from '../../../../assets/img/man.jpg'

function Post(props) {
    return (
        <div className={s.post}>
            <img src={man} alt="avatar"/>
            {props.message}
            <div>
                <span className={s.like}>{props.likesCount}</span>
            </div>
        </div>
    )
}

export default Post
