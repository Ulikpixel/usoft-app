import Posts from "./myPosts";
import {
  addPostAction,
  emptyInputAction,
  onPostChangeAction,
} from "../../../redux/actions/profile-actions";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    posts: state.profilePage.posts,
    newPostText: state.profilePage.newPostText,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onPostChange: (text) => {
      const action = onPostChangeAction(text);
      dispatch(action);
    },
    addPost: () => {
      dispatch(addPostAction());
      dispatch(emptyInputAction());
    },
  };
};

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);

export default PostsContainer;
