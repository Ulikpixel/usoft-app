import React, { useEffect } from "react";
import s from "./profile.module.css";
import PostsContainer from "./myPosts/myPostsContainer";
import Profileinfo from "./profileInfo/profileInfo";

function Profile(props) {
  const { getUsersProfile, match, getStatus } = props;
  useEffect(() => {
    let userID = match.params.userId;
    userID ? getUsersProfile(userID) : (userID = 2);
    getStatus(userID);
  }, [Profile]);
  return (
    <article className={s.profile}>
      <Profileinfo {...props} />
      <PostsContainer />
    </article>
  );
}

export default Profile;
