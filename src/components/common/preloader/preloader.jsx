import React from 'react';
import s from './preloader.module.css';

function Preloader() {
    return <img src='https://lh5.googleusercontent.com/proxy/kZtD76NTUJbCarWDT9uAMKjHSl7AlUIBig9hsyMTymtGZHto0yVbEazKjo1uiJfvfmv8Kl5xDGtoSbhSwIzNpx1L1-yQ56acCIpoB9Op5mRSIr86JcZ3_t2IwrO-wXSlew_h4cMmHmoKlEtlw_LLIEC_T0gxlzLdkVQq0qOyjw=s0-d' className={s.preloader} />;
}

export default Preloader;
