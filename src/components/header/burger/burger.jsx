import React from 'react';
import s from './burger.module.css';

function Burger({ open, setOpen }) {
    return (
        <div className={open ? `${s.burger} ${s.active}` : `${s.burger}`} 
                onClick={() => setOpen(!open)}>
            <span></span>
        </div>
    )
}

export default Burger
