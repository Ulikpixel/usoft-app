import { connect } from "react-redux";
import { getAuthUserData } from "../../redux/actions/header-actions";
import Header from "./header";

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.isAuth,
    login: state.auth.login,
  };
};

const HeaderContainer = connect(mapStateToProps, { getAuthUserData })(Header);

export default HeaderContainer;
