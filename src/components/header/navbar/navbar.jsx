import React from "react";
import s from "./navbar.module.css";
import { NavLink } from "react-router-dom";

function Navbar({ open, setOpen }) {
  return (
    <nav className={open ? `${s.menu} ${s.open_menu}` : s.menu}>
      <ul className={s.list}>
        <li className={s.item}>
          <NavLink
            to="/Profile"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            Profile
          </NavLink>
        </li>
        <li className={s.item}>
          <NavLink
            to="/Messages"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            Messages
          </NavLink>
        </li>
        <li className={s.item}>
          <NavLink
            to="/News"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            News
          </NavLink>
        </li>
        <li className={s.item}>
          <NavLink
            to="/Music"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            Music
          </NavLink>
        </li>
        <li className={s.item}>
          <NavLink
            to="/Users"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            Users
          </NavLink>
        </li>
        <li className={s.item}>
          <NavLink
            to="/Settings"
            onClick={() => setOpen(false)}
            className={s.link}
            activeClassName={s.active}
          >
            Settings
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
