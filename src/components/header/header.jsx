import React, { useState, useEffect } from "react";
import s from "./header.module.css";
import Navbar from "./navbar/navbar";
import Burger from "./burger/burger";
import { NavLink } from "react-router-dom";

function Header({ isAuth, login, getAuthUserData }) {
  const [open, setOpen] = useState(false);
  useEffect(() => {
    getAuthUserData()
  }, [Header]);
  return (
    <header className={s.header}>
      <div className={s.logo}>
        <p>
          {"<"}Usoft {"/>"}
        </p>
      </div>
      <Navbar open={open} setOpen={setOpen} />
      <div className={s.login}>
        {isAuth ? <p className={s.name}>{login}</p> : <NavLink to={"/login"}>login</NavLink>}
      </div>
      <Burger open={open} setOpen={setOpen} />
      <div
        className={open ? `${s.popup} ${s.active} ` : s.popup}
        onClick={() => setOpen(!open)}
      ></div>
    </header>
  );
}

export default Header;
