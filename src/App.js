import s from "./App.module.css";
import HeaderContainer from "./components/header/headerContainer";
import AuthRedirectComponent from "./components/profile/profileContainer";
import MessagesContainer from "./components/messages/messagesContainer";
import Settings from "./components/settings/settings";
import News from "./components/news/news";
import Music from "./components/music/music";
import Login from './components/login/login';
import { Route } from "react-router-dom";
import UsersContainer from "./components/users/usersContainer";

function App() {
  return (
    <div className={s.app}>
      <HeaderContainer />
      <main className={s.content}>
        <Route path="/profile/:userId?" component={AuthRedirectComponent} />
        <Route path="/messages" component={MessagesContainer} />
        <Route path="/news" component={News} />
        <Route path="/music" component={Music} />
        <Route path="/settings" component={Settings} />
        <Route path="/users" component={UsersContainer} />
        <Route path="/Login" component={Login}></Route>
      </main>
    </div>
  );
}

export default App;
